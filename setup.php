<?php

include_once 'dbConnect.php';

//delete all tables if they already exist
$conn->query('SET foreign_key_checks = 0');

if ($result = $conn->query("SHOW TABLES"))
{
    while($row = $result->fetch_array(MYSQLI_NUM))
    {
        $conn->query('DROP TABLE IF EXISTS '.$row[0]);
    }
}
$conn->query('SET foreign_key_checks = 1');

//create tables needed, please make sure db_user has sufficent permissions
$query = "CREATE TABLE users (
            id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
          user_email text NOT NULL
          );";
$conn->query($query);

$query = "CREATE TABLE sessions (
            id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
            cookie varchar(255) NOT NULL,
            authorized BOOL NOT NULL DEFAULT FALSE,
            user_id int NOT NULL,
            until int NOT NULL,
            auth_token varchar(255) NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
            UNIQUE (cookie, auth_token)
          );";
$conn->query($query);

$query = "CREATE TABLE codes (
            id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
            link text NOT NULL,
            short varchar(255) NOT NULL,
            user_id int NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE,
            UNIQUE (short)
          );";
$conn->query($query);

//TODO delete auto on run
 ?>
