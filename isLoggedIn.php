<?php
include_once 'checkLogin.php';

$output = array();

if($session !== null){
  $output['loggedIn'] = true;
  $output['userEmail'] = $session->user_email;
}else{
  $output['loggedIn'] = false;
}


die(json_encode($output));
?>
