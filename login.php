<?php

include_once 'dbConnect.php';
require_once 'config.php';

$output = array();


if(!isset($_POST['email']) || $_POST['email'] === ''){
  $output['error'] = 'missingParameters';
}else{

  $user_email_db = $conn->real_escape_string($_POST['email']);
  $sessionID_db = $conn->real_escape_string(uniqid());
  $auth_token_db = $conn->real_escape_string(md5(uniqid(rand(), true)));
  $until = time()+60*60*24*10;
  setcookie("qrityourselfsession", $sessionID_db, $until);

  $query = "INSERT IGNORE INTO users (user_email) VALUES ('$user_email_db');";
  $conn->query($query);

  $query = "SELECT * FROM users WHERE user_email = '$user_email_db';";
  $user = $conn->query($query)->fetch_object();

  $query = "INSERT INTO sessions (cookie, user_id, until, auth_token) VALUES ('$sessionID_db', $user->id, $until, '$auth_token_db')";
  $conn->query($query);



  $mail->Subject = 'login to qrityourself';

  $message = str_replace("SENDEREMAIL", $mail->Username, file_get_contents ( "templates/authorizeEmail.mustache" ));
  $message = str_replace("DATETIME", date('Y-m-d H:i:s'), $message);

  //get server url
  $url  = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
  $url .= $_SERVER['SERVER_NAME'];
  $url .= $_SERVER['REQUEST_URI'];

  $auth_url = dirname($url).'/authorize.php?session='.$sessionID_db.'&token='.$auth_token_db;

  $message = str_replace("AUTHLINK", $auth_url, $message);



  $mail->Body    = $message;


  $mail->addAddress($user_email_db);

  if(!$mail->send()) {

      $output['error'] = 'Mailer Error: ' . $mail->ErrorInfo;
  } else {
      $output['success'] = true;
  }
}
die(json_encode($output));


?>
