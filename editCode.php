<?php

include_once 'dbConnect.php';
include_once 'checkLogin.php';

$output = array();

if($session === null){
  $output['error'] = 'notLoggedIn';
}else{

  if(isset($_POST['link']) && isset($_POST['short']) && $_POST['link'] !== '' && $_POST['short'] !== ''){
    $link_db = $conn->real_escape_string($_POST['link']);
    $short_db = $conn->real_escape_string($_POST['short']);
    $user_id_db = $session->user_id;

    $query ="SELECT * FROM codes WHERE short = '$short_db';";
    $result = $conn->query($query);

    if($result->num_rows == 0){
      $query = "INSERT INTO codes (link, short, user_id) VALUES ('$link_db', '$short_db', $user_id_db)";

      $result = $conn->query($query);
      $output['result'] = 'inserted';
    }else{

      if($user_id_db != $result->fetch_object()->user_id){
        $output['error'] = 'permissionDenied';
      }else{
        $query = "UPDATE codes SET link='$link_db' WHERE short='$short_db';";
        $conn->query($query);
        $output['result'] = 'updated';
      }
    }
  }else{
    $output['error'] = 'missingParameters';
  }

}

die(json_encode($output));




?>
