//init
(function() {
    $('#edit-code-icon').on('click', function() {

    });

    $('#new-code-icon').on('click', function() {
        //load new code dialog
        $.ajax({
            url: "templates/newCode.mustache",
            success: function(template) {
                var rendered = Mustache.render(template);

                $("#main-container").html(rendered);
            }
        });
    });
})();
