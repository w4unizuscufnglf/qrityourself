$(document).ready(function() {
    //load main menu
    $.ajax({
        url: "templates/mainMenu.mustache",
        success: function(template) {
            var rendered = Mustache.render(template);
            $(rendered).appendTo("#main-container");
        }
    });

    //check if login needed
    $.ajax({
        url: "isLoggedIn.php",
        success: function(data) {
            var result;
            try {
                result = JSON.parse(data);
            } catch (e) {
                showError(e);
            }

            if ('error' in result) {
                showError(result.error);
            } else {
                if (!result.loggedIn) {
                    showLoginModal();

                }
            }


        }
    });

});

function showLoginModal() {

    $.ajax({
        url: "templates/loginModal.mustache",
        success: function(template) {
            var rendered = Mustache.render(template);
            $(rendered).appendTo("body");
            $('#login-modal').openModal({
                dismissible: false,
                opacity: 0.7,
            });

            $('#btn-login').on('click', function() {

                if ($('#login-email').hasClass('valid')) {
                    $.ajax({
                        url: "login.php",
                        type: 'POST',
                        data: {
                            email: $('#login-email').val()
                        },
                        success: function(data) {
                            var result;
                            try {
                                result = JSON.parse(data);
                            } catch (e) {
                                showError(e);
                            }

                            if ('error' in result) {
                                showError(result.error);
                            } else {
                                if (result.success) {
                                    $('#login-modal').closeModal();
                                    showWaitOnAuth();
                                }
                            }

                        }

                    });
                }
            });

        }
    });
}

function showWaitOnAuth() {
    $.ajax({
        url: "templates/waitOnAuth.mustache",
        success: function(template) {
            var rendered = Mustache.render(template);
            $(rendered).appendTo("body");
            $('#waitOnAuth-modal').openModal({
                dismissible: false,
                opacity: 0.7,
            });

            var checkAuthInterval = setInterval(function() {
                $.ajax({
                    url: "isLoggedIn.php",
                    success: function(data) {

                        var result;
                        try {
                            result = JSON.parse(data);
                        } catch (e) {
                            showError(e);
                        }

                        if ('error' in result) {
                            showError(result.error);
                        } else {
                            if (result.loggedIn) {
                                clearInterval(checkAuthInterval);
                                $('#waitOnAuth-modal').closeModal();
                                Materialize.toast('Successfully logged in', 4000);
                            }
                        }

                    }
                });
            }, 1000);
        }
    });
}

function showError(text) {
    console.log('Shit: ' + text);
    Materialize.toast('Shit: ' + text, 4000);
}
