//init
(function() {

    Materialize.updateTextFields(); //fix overlapping on placeholder input

    //intit qr code preview

    //determine current folder for url
    var url = window.location.href;

    var myFolder_http = url.substring(0, url.lastIndexOf('/') + 1);
    var myFolder = myFolder_http.split('http://')[1];
    //TODO

    //init
    renderQr(myFolder);
    $('#qr-code-label').text(myFolder);
    $('#qr-code-label').attr('href', myFolder_http);



    $('#qr-link').focusout(function() {
        //add http to link if it not already contains it, for html5 validate
        if (($(this).val().toLowerCase().indexOf("http://") < 0) && ($(this).val().toLowerCase().indexOf("https://") < 0)) {
            $(this).val('http://' + $(this).val());
        }

    });
    //on input handler for link changes
    $('#link-text').on("keyup", function() {
        $('#qr-code-label').text(myFolder + $(this).val());
        $('#qr-code-label').attr('href', myFolder_http + $(this).val());
        renderQr(myFolder_http + $(this).val());
        $('#btn-save').removeClass('green');
        $('#btn-save').addClass('red');
    });

    $('#qr-link').keypress(function() {
        $('#btn-save').removeClass('green');
        $('#btn-save').addClass('red');
    });

    $('#btn-print').on('click', function() {
        window.print();
    });

    $('#btn-save').on('click', function() {
        //TODO
        if ($('#link-text').val().length == 0) {
            $('#link-text').addClass('invalid');
        }

        if ($('#qr-link')[0].checkValidity() && $('#link-text')[0].checkValidity()) {
            saveCode();
        }

    });

})();

function saveCode() {
    $.ajax({
        url: "editCode.php",
        type: 'POST',
        data: {
            short: $('#link-text').val(),
            link: $('#qr-link').val()
        },
        success: function(data) {
            var result;
            try {
                result = JSON.parse(data);
            } catch (e) {
                showError(e);
            }

            if ('error' in result) {
                showError(result.error);
            } else {
                console.log(result.result);
                $('#btn-save').removeClass('red');
                $('#btn-save').addClass('green');

            }

        }

    });
}

function renderQr(text) {
    //default options for qr code rendering, refer https://larsjung.de/jquery-qrcode/
    qrcodeOptions = {
        render: 'image',

        // version range somewhere in 1 .. 40
        minVersion: 1,
        maxVersion: 40,

        // error correction level: 'L', 'M', 'Q' or 'H'
        ecLevel: 'L',

        // offset in pixel if drawn onto existing canvas
        left: 0,
        top: 0,

        // size in pixel
        size: 400,

        // code color or image element
        fill: '#000',

        // background color or image element, null for transparent background
        background: null,

        // corner radius relative to module width: 0.0 .. 0.5
        radius: 0.5,

        // quiet zone in modules
        quiet: 0,

        // modes
        // 0: normal
        // 1: label strip
        // 2: label box
        // 3: image strip
        // 4: image box
        mode: 0,

        mSize: 0.1,
        mPosX: 0.5,
        mPosY: 0.5,

        label: 'no label',
        fontname: 'sans',
        fontcolor: '#000',

        image: null
    };

    qrcodeOptions.text = text;
    $('#qr-code-preview').empty();
    $('#qr-code-preview').qrcode(qrcodeOptions);
    $('#qr-code-preview > img').addClass('responsive-img'); //make qr code image responsive TODO

    //set download for btn-download-png

    $('#btn-download-png').attr('download', 'qrityourselfcode.png');
    $('#btn-download-png').attr('href', $('#qr-code-preview > img').attr('src'));

}
