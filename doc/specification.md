# Pflichtenheft

## Arbeitstitel: qrityourself
  Loris Schönegger
## Beschreibung des Projektes
![Konzept eines URL-Shorteners](img/konzept-url-shortener.svg.png)
Konzept eines URL-Shorteners


- Ein URL-Shortener ist ein Tool, welches ein gegebenes Kürzel auf eine bestimmte URL-Zeigen lässt (Beispiel Abb.1)
- Ein *Short* ist ein Kürzel, welches, angehängt an die Domain der Seite, bei Aufruf auf einen bestimmten Link zeigt.

- Dieser Link kann unabhängig von dem Short auch im Nachhinein geändert werden.


Dieses verfahren ist insbesondere bei QR-Codes vorteilhaft, da auf diese Weise weniger Information in den Code gepresst werden muss.

---
## Zielbestimmung und Produktübersicht

- Erstellung einer Link-shortener/QR-Code Generator Webapp zum Hosten auf Apache2 Servern.

- User sollen so ihren eigenen Shortener auf eigener Domain hosten können, auch parallel zu bereits bestehenden sites oder Webapps auf dem gleichen Server/ der gleichen Domain.
- Das System soll vollständig ohne Passwörter funktionieren, es sollen Anfragen per Email zur Authorisierung versendet werden
- Authentifizierte Benutzer sollen die eigenen Links ändern können
- Es sollen Zugriffsstatistiken (Gesamt-Klicks) der einzelnen Links abrufbar sein

- Der Hauptgrund für die Entwicklung ist es eine unabhängige und simple Alternative zu Diensten wie _bit.ly_ oder goo.gl zu schaffen.
- Vorallem die benutzerdefinierte Wahl des Shortener-Kürzels soll das Projekt von den bereits verfügbaren abheben

---
## Zielgruppe und Anforderungen

Die Webapp wird zum Kürzen von URLs und zum Erstellen von QR-Codes, welche auf diese Links zeigen eingesetzt.

Zur Zielgruppe ist jede Person zu zählen, die die folgend aufgeführten Kentnisse besitzt und Links kürzen will.

### Hosting
- Die Zielgruppe sind Administratoren welche:
  1. die eigene Domain für Shortening nutzen wollen

  2. die Kentnisse besitzen, einen eigenen Apache-Server zu betreiben (Konfiguration von rewriteEngine und VirtualHosts)

### Endbenutzer
  - Von den Endbenutzern wird die Fähigkeit mit URLs und QR-Codes umgehen zu können erwartet.
  - Sie sollten auch mit ihrem email-Account umgehen können.
  - Die App benötigt eine ständige Internetverbindung undsoll auf folgenden Browsern laufen:

    Chrome 35+, Firefox 31+, Safari 7+, IE 10+

---

## Produktfunktionen

![Use Case Diagramm des ersten Prototyps](img/useCase.png)

Use Case Diagramm des ersten Prototyps

- Der Administrator soll, neben der Konfiguration des Servers wie sie in README.md beschrieben wird nur eine entsprechendes Setup-Script ausführen müssen.
- Der User kann die im UseCase Diagramm dahrgestellten Funktionen nutzen



Auf eine detaillierte Beschreibung der Funktionen wird an dieser Stelle vorerst verzichtet, sie sind noch in Ausarbeitung

---

## Prototyp der Benutzeroberfläche

![Prototyp UI Neuer Link](img/newLinkPrototype.png)

Prototyp UI Neuer Link


- Die Benutzerinteraktion mit der UI beläuft sich auf ein Minimum, es folgt das Activity Diagramm des  Konzepts des Erstellen eines Links

![Activity Diagramm Erstellung eines Links](img/activity.png)

Activity Diagramm Erstellung eines Links



## Zugriffsrechte

- Der Administrator installiert die Webapp auf dem Server und muss ab diesem Zeitpunkt nicht mehr in den Prozess eingreifen.
- Der Administrator kann auf Datenbankebene Daten löschen bzw. bearbeiten. (SQL Console)


- Die Nutzer authentifizieren sich bei jedem login mittels klick auf den Button in der erhaltenen Email und bleiben mittels Cookies angemeldet.
  - Jeder Nutzer kann beliebig viele Links erstellen, er kann jedoch nur seine eigenen bearbeiten bzw. keine schon bestehenden von Anderen bearbeiten


  ![Prototyp Login System Email Eingabe](img/loginPrototype.png)

  Prototyp Login System Email Eingabe



  ![Prototyp Login Bestätigungs Email](img/emailPrototype.png)

  Prototyp Login  Bestätigungs Email

## Rechtliche Anforderungen
   EU Richtlinien verlangen eine Cookie-Warnung, Datenschutzaufklärung und ein Impressum

## Milestones

- Die Milestones werden im ersten Schritt nach Fertigstellung dieses Pflichtenheftes verfeinert.

![Milestones vorläufig](img/milestones.png)

Milestones vorläufig
